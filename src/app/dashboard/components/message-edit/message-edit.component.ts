import {Component, OnInit} from '@angular/core';
import {Message} from '../../models/message.model';
import {ActivatedRoute, Router} from '@angular/router';
import {DashboardService} from '../../services/dashboard.service';

@Component({
  selector: 'app-message-edit',
  templateUrl: './message-edit.component.html',
})
export class MessageEditComponent implements OnInit {
  public message: Message;
  public returnUrl: string;
  private oldMessage: string;

  constructor(private router: Router, private route: ActivatedRoute, private service: DashboardService) {
  }

  ngOnInit() {
    const id = this.route.snapshot.params.id;
    const name = this.route.snapshot.params.name;
    this.message = new Message(id, name);
    this.oldMessage = this.message.Name;
    this.returnUrl = `/dashboard`;
  }

  public save() {
    this.service.update(this.oldMessage, this.message.Name).subscribe(
      () => this.router.navigate([this.returnUrl])
    );
  }
}
