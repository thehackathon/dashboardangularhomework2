import {Component, OnInit} from '@angular/core';
import {DashboardService} from '../../services/dashboard.service';
import {Message} from '../../models/message.model';
import {Router} from '@angular/router';
import {map, skipWhile} from 'rxjs/operators';
import {fromEvent} from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: `dashboard.component.html`,
})
export class DashboardComponent implements OnInit {
  constructor(private service: DashboardService, private router: Router) {
  }

  private static _id = 0;
  private static _deleteMessage = 'Do you want to delete this message?';
  public modalMessage: string;
  public isModalOpened: boolean;
  public messages: Message[] = [];
  public modalArg: object;

  ngOnInit() {
    this.isModalOpened = false;
    this.service.get()
      .pipe(
        map(messagesString => {
          const result: Message[] = [];
          for (const msgString of messagesString) {
            const msg = new Message(++DashboardComponent._id, msgString);
            result.push(msg);
          }
          return result;
        })
      )
      .subscribe(
        (result) => {
          console.log(1);
          this.messages = result;
        },
      );
  }

  public delete(id: number) {
    const msgIdx = this.messages.findIndex(m => m.Id === id);
    this.service.delete(this.messages[msgIdx].Name)
      .subscribe(() => {
        this.messages.splice(msgIdx, 1);
        this.closeModal();
      });
  }

  public openModal(id: number) {
    this.modalArg = {id};
    this.modalMessage = DashboardComponent._deleteMessage;
    this.isModalOpened = true;
  }

  public closeModal() {
    this.isModalOpened = false;
  }
}
