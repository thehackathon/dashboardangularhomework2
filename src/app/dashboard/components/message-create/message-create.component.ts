import {Component, OnInit} from '@angular/core';
import {Message} from '../../models/message.model';
import {ActivatedRoute, Router} from '@angular/router';
import {DashboardService} from '../../services/dashboard.service';

@Component({
  selector: 'app-message-create',
  templateUrl: './message-create.component.html',
})
export class MessageCreateComponent implements OnInit {
  public message: Message;
  public returnUrl: string;

  constructor(private router: Router, private route: ActivatedRoute, private service: DashboardService) {
  }

  ngOnInit() {
    this.returnUrl = `/dashboard`;
    this.message = new Message(0, '');
  }

  public save() {
    this.service.add(this.message.Name).subscribe(
      () => this.router.navigate([this.returnUrl])
    );
  }
}
