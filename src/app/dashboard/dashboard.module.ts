import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoreModule} from '../core/core.module';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {DashboardService} from './services/dashboard.service';
import {HttpClientModule} from '@angular/common/http';
import {ShareModule} from '../share/share.module';
import {MessageCreateComponent} from './components/message-create/message-create.component';
import {MessageEditComponent} from './components/message-edit/message-edit.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    DashboardRoutingModule,
    CoreModule,
    ShareModule,
    RouterModule,
  ],
  declarations: [
    DashboardComponent,
    MessageCreateComponent,
    MessageEditComponent,
  ],
  providers: [DashboardService]
})
export class DashboardModule {
}
