import {FormValueModel} from '../../share/form/models/form-value.model';

export class Message implements FormValueModel {
  private _id: number;
  private _name: string;

  public get Id(): number {
    return this._id;
  }

  public set Id(val: number) {
    this._id = val;
  }

  public get Name(): string {
    return this._name;
  }

  public set Name(val: string) {
    this._name = val;
  }

  constructor(id: number, name: string) {
    this._id = id;
    this._name = name;
  }
}
