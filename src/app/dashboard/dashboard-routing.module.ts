import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {DashboardComponent} from './components/dashboard/dashboard.component';
import {MessageCreateComponent} from './components/message-create/message-create.component';
import {MessageEditComponent} from './components/message-edit/message-edit.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent
  },
  {
    path: 'new',
    component: MessageCreateComponent
  },
  {
    path: 'edit',
    component: MessageEditComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class DashboardRoutingModule {
}
