import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class DashboardService {
  private readonly apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = 'http://localhost:51764/api/values';
  }

  get(): Observable<string[]> {
    return this.http
      .get<string[]>(this.apiUrl);
  }

  add(message: string): Observable<void> {
    return this.http
      .post<void>(this.apiUrl, JSON.stringify(message), {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  update(oldMessage: string, newMessage: string): Observable<void> {
    return this.http
      .put<void>(this.apiUrl, {oldMessage, newMessage});
  }

  delete(message): Observable<void> {
    return this.http
      .delete<void>(this.apiUrl, {
        params: {message: message}
      });
  }
}
