import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {MenuItem} from './models/menu-item.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public menu: MenuItem[] = [];
  public selected: MenuItem;

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.menu.push(new MenuItem('Tasks', '/todo'));
    this.menu.push(new MenuItem('Dashboard', '/dashboard'));

    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.selected = this.menu.find(m => m.Url === this.router.url);
      }
    });
  }
}
