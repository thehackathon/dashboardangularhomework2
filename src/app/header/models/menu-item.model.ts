export class MenuItem {
  private readonly _title: string;
  public get Title(): string {
    return this._title;
  }

  private readonly _url: string;
  public get Url(): string {
    return this._url;
  }

  constructor(title, url) {
    this._title = title;
    this._url = url;
  }
}
