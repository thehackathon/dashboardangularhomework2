import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ToDo} from '../../models/todo.model';
import {ToDoService} from '../../services/todo.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styles: [`
      .test-class {
          background-color: #ff9e9e;
      }
  `]
})
export class ToDoListComponent implements OnInit {
  public list: ToDo[];
  public today: Date = new Date();
  public todayCondition = false;

  constructor(private router: Router, private todoService: ToDoService) {
  }

  ngOnInit() {
    this.list = this.todoService.getAll();
  }

  public edit(id: number) {
    this.router.navigate([`/todo/${id}`]);
  }

  public add() {
    this.router.navigate([`/todo/new`]);
  }

  public delete(id: number) {
    this.todoService.delete(id);
    this.list.splice(this.list.findIndex(item => item.Id === id), 1);
  }
}
