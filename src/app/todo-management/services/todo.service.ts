import {Injectable} from '@angular/core';
import {ToDo} from '../models/todo.model';

@Injectable()
export class ToDoService {
  private key = 0;
  private collection_name = 'ToDoList';

  constructor() {
    const collection: ToDo[] = [];
    collection.push({Id: 0, Name: 'Initialize a list'});
    collection.push({Id: 1, Name: 'Output what\'s in the list'});
    this.key = 2;
    localStorage.setItem(this.collection_name, JSON.stringify(collection));
  }

  get(id: number): ToDo {
    const collection = JSON.parse(localStorage.getItem(this.collection_name));
    return collection.find(t => t.Id === +id);
  }

  getAll(): ToDo[] {
    return JSON.parse(localStorage.getItem(this.collection_name));
  }

  post(todo: ToDo): number {
    const collection = JSON.parse(localStorage.getItem(this.collection_name));
    todo.Id = this.key++;
    collection.push(todo);
    localStorage.setItem(this.collection_name, JSON.stringify(collection));
    return todo.Id;
  }

  put(todo: ToDo) {
    const collection = JSON.parse(localStorage.getItem(this.collection_name));
    const itemIdx = collection.findIndex(t => t.Id === todo.Id);
    collection[itemIdx] = todo;
    localStorage.setItem(this.collection_name, JSON.stringify(collection));
  }

  delete(id: number): void {
    const collection = JSON.parse(localStorage.getItem(this.collection_name));
    collection.splice(collection.findIndex(item => item.Id === id), 1);
    localStorage.setItem(this.collection_name, JSON.stringify(collection));
  }
}
