import {FormValueModel} from '../../share/form/models/form-value.model';

export class ToDo implements FormValueModel {
  Id: number;
  Name: string;
}
