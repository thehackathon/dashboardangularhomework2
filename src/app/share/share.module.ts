import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormComponent} from './form/form.component';
import {FormsModule} from '@angular/forms';
import {CoreModule} from '../core/core.module';
import {ModalComponent} from './modal/modal.component';

@NgModule({
  declarations: [
    FormComponent,
    ModalComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    CoreModule,
  ],
  exports: [
    FormComponent,
    ModalComponent,
  ]
})
export class ShareModule {
}
