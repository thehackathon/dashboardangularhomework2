import {Component, EventEmitter, OnInit, Output, Input} from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
})
export class ModalComponent implements OnInit {
  @Input() text: string;
  @Input() btnText: string;
  @Input() arg: object;
  @Output() action: EventEmitter<object> = new EventEmitter<object>();
  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();

  constructor() {
  }

  ngOnInit() {
  }

  public close() {
    this.cancel.emit();
  }

  public onAction() {
    this.action.emit(this.arg);
  }
}
