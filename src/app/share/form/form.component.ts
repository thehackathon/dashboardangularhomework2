import {Component, Input, EventEmitter, Output} from '@angular/core';
import {Router} from '@angular/router';
import {FormValueModel} from './models/form-value.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
})
export class FormComponent<T extends FormValueModel> {
  @Input() val: T;
  @Input() returnUrl: string;
  @Input() title: string;
  @Output() save: EventEmitter<T> = new EventEmitter<T>();

  constructor(private router: Router) {
  }

  public onSubmit() {
    this.save.emit(this.val);
  }

  public onBack() {
    this.router.navigate([this.returnUrl]);
  }
}
